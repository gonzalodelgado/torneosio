import wtforms


FORMATO_FECHA = '%d/%m/%Y'
ELIMINACION = 'eliminacion'
TODOS_CONTRA_TODOS = 'todos_contra_todos'
SISTEMAS_CHOICES = (
    (ELIMINACION, 'Eliminaci&oacute;n Simple'),
    (TODOS_CONTRA_TODOS, 'Todos contra Todos')
)
TIEMPOS = 40, 90, 120


def es_anterior_a(nombre_campo_a_comparar):
    def validator(form, campo):
        campo_a_comparar = form[nombre_campo_a_comparar]
        if campo.data >= campo_a_comparar.data:
            mensaje = 'Debe ser anterior a %s' % campo_a_comparar.label.text
            raise wtforms.validators.ValidationError(mensaje)
    return validator


class TorneoForm(wtforms.Form):
    desde = wtforms.DateField(
        'Desde',[wtforms.validators.required(), es_anterior_a('hasta')],
        format=FORMATO_FECHA)
    hasta = wtforms.DateField(
        'Hasta', [wtforms.validators.required()], format=FORMATO_FECHA)
    lugar = wtforms.StringField(
        'Los encuentros se llevar&aacute;n a cabo en',
        [wtforms.validators.required()])
    equipos = wtforms.IntegerField(
        'Cantidad de equipos participantes',
        [wtforms.validators.required(), wtforms.validators.number_range(min=2)])
    canchas = wtforms.IntegerField(
        'Cantidad de canchas disponibles',
        [wtforms.validators.required(), wtforms.validators.number_range(min=1)])
    sistema = wtforms.RadioField(
        'Se jugar&aacute; con el sistema:',
        [wtforms.validators.required()],
        choices=SISTEMAS_CHOICES)
    tiempo_partido = wtforms.SelectField(
        'Tiempo de cada partido',
        [wtforms.validators.required()], coerce=int,
        choices=[(t, '%d minutos' % t) for t in TIEMPOS])

    class Meta:
        locales = ['es_AR', 'es']
