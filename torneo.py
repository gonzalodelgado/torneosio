import os
import re
from itertools import chain
from operator import itemgetter
from tempfile import NamedTemporaryFile

from glpk import LPX
from flask import Flask, request, render_template

from form import TorneoForm, ELIMINACION

app = Flask(__name__)
HORAS_DIA = 10
solucion_rg = re.compile(
    r'x\[(?P<cancha>\d+),(?P<turno>\d+),(?P<local>\d+),(?P<visitante>\d+)\]')


def calcular_equipos_libres(participantes):
    for i in range(1, participantes):
        pot = 2 ** i
        if pot >= participantes:
            return pot - participantes
    return 0

def generar_fixture(participantes):
    fixture = []
    equipos_iniciales = range(1, participantes + 1) # Lista de equipos
    i = j = 0   # Pivots de equipos
    ganadores = 0   # Acumulador de ganadores
    while participantes >= 2:
        libres = calcular_equipos_libres(participantes)
        juegan = participantes - libres
        for k in range(ganadores, juegan + ganadores):
            equipos_iniciales.append(chr(ord('a') + k))
            ganadores += 1
        i = j
        j += juegan
        equipos = equipos_iniciales[i:j]
        partidos = [
            (equipos[k], equipos[k + 1]) for k in range(0, len(equipos), 2)]
        fixture.append(partidos)
        participantes = juegan / 2 + libres
    return fixture

def resolver(equipos, canchas, turnos, sistema='eliminacion'):
    tmp = NamedTemporaryFile()
    tmp.file.write('param CD := %d;\n' % canchas)
    tmp.file.write('param E := %d;\n' % equipos)
    tmp.file.write('param T := %d;\n' % turnos)
    tmp.file.seek(0)
    lp = LPX(gmp=('%s.mod' % sistema, tmp.name))
    lp.scale()
    lp.adv_basis()
    lp.simplex()
    lp.integer()
    soluciones = [solucion_rg.match(c.name).groupdict()
                  for c in lp.cols if c.primal]
    tmp.file.close()
    return soluciones

def resolver_eliminacion(datos):
    contador_partidos = 0
    fechas = []
    fixture = generar_fixture(datos['equipos'])
    for fecha in fixture:
        lista_equipos = list(chain(*fecha))
        partidos = resolver(
            equipos=len(fecha) * 2, canchas=datos['canchas'],
            turnos=(HORAS_DIA * 60) / datos['tiempo_partido'],
            sistema=datos['sistema'])
        for partido in partidos:
            partido['id'] = chr(ord('a') + contador_partidos)
            partido['local'] = lista_equipos[int(partido['local']) - 1]
            partido['visitante'] = lista_equipos[int(partido['visitante']) - 1]
            contador_partidos += 1
        fechas.append(partidos)
    return fechas

@app.route('/', methods=['GET', 'POST'])
def torneo():
    contexto = {}
    form = TorneoForm(request.form or None)
    if request.method == 'POST' and form.validate():
        if form.data['sistema'] == ELIMINACION:
            contexto['soluciones'] = resolver_eliminacion(form.data)
        else:
            contexto['soluciones'] = resolver(
                equipos=form.data['equipos'],
                canchas=form.data['canchas'],
                turnos=(HORAS_DIA * 60) / form.data['tiempo_partido'],
                sistema=form.data['sistema'])
    contexto['form'] = form
    return render_template('torneo.html', **contexto)


if __name__ == '__main__':
    port = int(os.environ.get("PORT", 5000))
    app.run(host='0.0.0.0', port=port, debug=True)
