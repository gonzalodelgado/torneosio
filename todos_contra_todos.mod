/* Torneo todos contra todos */

/* Autora: Fátima María Laura Moises */

/* Los Recursos que tengo son las canchas disponibles “CD” en las que se llevarán
   a cabo los encuentros; los días de torneo “DT” (1, 2, 3, …, n);  las adaptaciones
   especiales para este torneo será la duración de cada partido “TP” para realizar
   los partidos, con una duración maxima de 120 minutos y con la flexibilidad de
   cargar la cantidad de canchas disponibles “CD” para aprovechar las instalaciones
   al máximo.
   Teniendo en cuenta de que en varias canchas se pueden jugar paralelamente el mismo
   turno dependiendo de la disponibilidad de ellas, el sistema definirá un turno para
   realizar los encuentros simultáneamente. */ 
 
param CD, integer, > 0, default 4;
/* canchas disponibles */

param T, integer, > 0, default 6;
/* turnos por cancha */

param E, integer, > 0, default 6;
/* equipos participantes */

var x{1..CD, 1..T, 1..E, 1..E}, binary;
/* cada variable determina si se juega o no un partido entre el equipo local l y el equipo visitante */

maximize z: sum{c in 1..CD, t in 1..T, l in 1..E, v in 1..E} x[c,t,l,v];

s.t. r1{c in 1..CD, t in 1..T, e in 1..E}: x[c,t,e,e] = 0;
/* ningún equipo juega contra sí mismo */

s.t. r2{a in 1..E, b in 1..E}: sum{c in 1..CD, t in 1..T} x[c,t,a,b] + sum{c in 1..CD, t in 1..T} x[c,t,b,a] <= 1;
/* cada par de equipos se enfrenta, a lo sumo, una sola vez en todo el torneo */

s.t. r3{c in 1..CD, t in 1..T}: sum{l in 1..E, v in 1..E} x[c,t,l,v] <= 1;
/* en cada cancha, en cada turno, a lo sumo se juega un sólo partido */

s.t. r4a{e in 1..E}: sum{c in 1..CD, t in 1..T, v in 1..E} x[c,t,e,v] - sum{c in 1..CD, t in 1..T,l in 1..E} x[c,t,l,e] <= 1;
/* cada equipo juega aproximadamente la misma cantidad de veces como local y como visitante */

s.t. r4b{e in 1..E}: sum{c in 1..CD, t in 1..T, l in 1..E} x[c,t,l,e] - sum{c in 1..CD, t in 1..T,v in 1..E} x[c,t,e,v] <= 1;
/* (complementa restricción anterior r4a) */

solve;


printf "T C L V\n";
for {t in 1..T}
{ for {c in 1..CD}
        {
        printf "%d %d %d %d\n", t, c, sum{l in 1..E, v in 1..E} l * x[c,t,l,v], sum{l in 1..E, v in 1..E} v * x[c,t,l,v]; 
        }
}

end;
